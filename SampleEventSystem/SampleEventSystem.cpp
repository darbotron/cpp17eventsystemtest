// ConsoleApplication5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "GCTypeId.h"
#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <type_traits>

using namespace std;

class IListener;
class CEventManager;


//----------------------------------------------------------------------
class CEventRegistryEntry
{
	friend class CEventManager;

	string						m_strName;
	GCTypeID					m_tidEventDataType;
	vector< const IListener* >	m_vecpListeners;

	CEventRegistryEntry() = delete;

	CEventRegistryEntry( const string& rstrName, GCTypeID tidDataType )
	: m_strName			( rstrName )
	, m_tidEventDataType( tidDataType )
	{}

	bool NameIs( const string& strName )
	{
		return ( m_strName.compare( strName ) == 0 );
	}

	bool EventDataTypeIDIs( GCTypeID tidToCheck )
	{
		return tidToCheck == m_tidEventDataType;
	}

	void AddListener( const IListener* pListenerToAdd )
	{
		m_vecpListeners.push_back( pListenerToAdd );
	}

	void Broadcast( GCTypeID tidOfData, void* pData );
};


//----------------------------------------------------------------------
class IListener
{
	friend class CEventRegistryEntry;
	virtual void VOnEventReceived( GCTypeID tidOfData, void* pData ) const = 0;
};


//----------------------------------------------------------------------
template< typename TPointerDataType >
class TListenerDerivedEventDataIsPointer
: public IListener
{
	friend class CEventManager;

	function< void( TPointerDataType ) > m_fnListenerFunction;


	TListenerDerivedEventDataIsPointer( function< void( TPointerDataType ) > fnListerFunction )
	{
		static_assert( ( true == is_pointer< TPointerDataType >::value ), "cannot be instantiated with non-pointer data type" );
		m_fnListenerFunction = fnListerFunction;
	}

	virtual void VOnEventReceived( GCTypeID tidOfData, void* pData ) const override
	{
		GCASSERT( tidOfData == GetGCTypeIDOf( TPointerDataType ), "type mismatch when recieving the evnet data, this SHOULD be impossible" );
		m_fnListenerFunction( reinterpret_cast< TPointerDataType >( pData ) );
	}
};


//----------------------------------------------------------------------
template< typename TValueOrRefDataType >
class TListenerDerivedEventDataIsValue
: public IListener
{
	friend class CEventManager;

	using TValueDataType = typename remove_reference< TValueOrRefDataType >::type;

	function< void( TValueOrRefDataType ) > m_fnListenerFunction;

	TListenerDerivedEventDataIsValue( function< void( TValueOrRefDataType ) > fnListerFunction )
	{
		static_assert( ( false == is_pointer< TValueDataType >::value ), "cannot be instantiated with pointer data type" );
		m_fnListenerFunction = fnListerFunction;
	}

	virtual void VOnEventReceived( GCTypeID tidOfData, void* pData ) const override
	{
		GCASSERT( tidOfData == GetGCTypeIDOf( TValueDataType ), "type mismatch when recieving the event data, this SHOULD be impossible" );
		m_fnListenerFunction( *( reinterpret_cast< TValueDataType* >( pData )) );
	}
};

////////////////////////////////////////////////////////////////////////
class CEventManager
{
	vector< CEventRegistryEntry* > m_vecpEvents;

	//------------------------------------------------------------------
	CEventRegistryEntry* FindEvent( const string& rstrName )
	{
		for( CEventRegistryEntry* pEvent : m_vecpEvents )
		{
			if( pEvent->NameIs( rstrName ) )
			{
				return pEvent;
			}
		}
		return nullptr;
	}

public:

	enum class EEventRegisterStatus
	{
		NotRegistered,
		Registered,
		NameRegisteredDataMismatch,
	};

	enum class EEventListenError
	{
		OK,
		NameRegisteredDataMismatch,
	};

	//------------------------------------------------------------------
	EEventRegisterStatus GetEventRegisteredStatus( const string& rstrName, GCTypeID tidEventData )
	{
		CEventRegistryEntry* pEvent = FindEvent( rstrName );

		if( nullptr == pEvent )
		{
			return EEventRegisterStatus::NotRegistered;
		}

		if( pEvent->EventDataTypeIDIs( tidEventData ) )
		{
			return EEventRegisterStatus::Registered;
		}
		return EEventRegisterStatus::NameRegisteredDataMismatch;		
	}

	//------------------------------------------------------------------
	EEventRegisterStatus RegisterEvent( const string& rstrName, GCTypeID tidEventData )
	{
		switch( GetEventRegisteredStatus( rstrName, tidEventData ) )
		{
		case EEventRegisterStatus::NotRegistered:
			{
				CEventRegistryEntry* pEvent = new CEventRegistryEntry( rstrName, tidEventData );
				m_vecpEvents.push_back( pEvent );
			}
			break;

		case EEventRegisterStatus::NameRegisteredDataMismatch:
			return EEventRegisterStatus::NameRegisteredDataMismatch;
		}

		return EEventRegisterStatus::Registered;
	}

private:

	//------------------------------------------------------------------
	void BroadcastEvent( const string& rstrName, GCTypeID tidDataType, void* pDataCastToVoid )
	{
		for( CEventRegistryEntry* pEvent : m_vecpEvents )
		{
			if( pEvent->NameIs( rstrName ) )
			{
				pEvent->Broadcast( tidDataType, pDataCastToVoid );
				break;
			}
		}
	}

public:
	//------------------------------------------------------------------
	template < typename TValueDataType >
	void BroadcastEvent( const string& rstrName, TValueDataType tData )
	{
		static_assert( ( false == is_pointer< TValueDataType >::value ), "cannot be used with pointer data types" );
		BroadcastEvent( rstrName, GetGCTypeIDOf( TValueDataType ), reinterpret_cast< void* >( &tData ) );
	}

	//------------------------------------------------------------------
	template < typename TPointerDataType >
	void BroadcastEvent( const string& rstrName, TPointerDataType* pData )
	{
		static_assert( ( true == is_pointer< TPointerDataType* >::value ), "cannot be used with non-pointer data types" );
		BroadcastEvent( rstrName, GetGCTypeIDOf( TPointerDataType* ), reinterpret_cast<void*>( pData ) );
	}

	//------------------------------------------------------------------
	// this function should be refactored: 
	// * need some way to remove listeners, recommend to return a "handle" 
	//	 (i.e. struct containing a private pointer to the listener?)
	// 
	// * will need different internal flow maybe remove the check for 
	//   event being registered already?
	//------------------------------------------------------------------
	template < typename TFunctorPointerParam >
	EEventListenError AddListener( const string& rstrName, TFunctorPointerParam fnListenerFunction )
	{
		using TFunctionTraits = typename GCHelpers::function_traits< TFunctorPointerParam >;
		using TFunctionParam0 = typename TFunctionTraits::template argument< 0 >;

		GCTypeID tidOfDataExpectedByFunction = GetGCTypeIDOf( TFunctionParam0 );

		switch( GetEventRegisteredStatus( rstrName, tidOfDataExpectedByFunction ) )
		{
		case EEventRegisterStatus::NotRegistered:
			RegisterEvent( rstrName, tidOfDataExpectedByFunction );
			// deliberate drop through

		case EEventRegisterStatus::Registered:
			{
				GCTypeID				tidFunctorData	= GetGCTypeIDOf( TFunctionParam0 );
				
				IListener*				pListener = nullptr;

				// "if constexpr" is pretty much a template level compile time #define 
				// since the test in the braces must be a "const expr" (i.e. compile time constant)
				// this allows the compiler to "throw away" either the if{ ... } or the  else{ ... }
				// which means that you can do stuff like this (which wouldn't compile otherwise!)
				if constexpr ( is_pointer< TFunctionParam0 >::value )
				{
					pListener = new TListenerDerivedEventDataIsPointer< TFunctionParam0 >( fnListenerFunction );
				}
				else
				{
					pListener = new TListenerDerivedEventDataIsValue< TFunctionParam0 >( fnListenerFunction );
				}

				CEventRegistryEntry* 	pEvent = FindEvent( rstrName );
				pEvent->AddListener( pListener );
			}
			return EEventListenError::OK;
		}

		return EEventListenError::NameRegisteredDataMismatch;
	}
};


//----------------------------------------------------------------------
// functions from classes
//----------------------------------------------------------------------
void CEventRegistryEntry::Broadcast( GCTypeID tidOfData,  void* pData )
{
	for( const IListener* pListener : m_vecpListeners )
	{
		pListener->VOnEventReceived( tidOfData, pData );
	}
}


//----------------------------------------------------------------------
// file scope functions just for testing
void TestEventCBIntPtr( int* pInteger )
{			
	cout << "Callback: pInteger: " << ( *pInteger ) << endl;
} 

void TestEventCBFloat( float fLoat )
{			
	cout << "Callback: fLoat: " << fLoat << endl;
} 

void TestEventCBIntRef( int& riInteger )
{			
	cout << "Callback: riInteger: " << riInteger << endl;
} 


//----------------------------------------------------------------------
int main()
{
	CEventManager		cEventManager;
	int					iData	= 666;
	int*				piData	= &iData;
	int&				riData	= iData;

	// register event, add listener and send
	cEventManager.RegisterEvent( "dave", GetGCTypeIDOf( int* ) );

	cEventManager.AddListener( "dave", TestEventCBIntPtr );

	cEventManager.AddListener
	(
		"dave",
		[]( int* pInteger )
		{			
			cout << "Lambda: pInteger: " << ( *pInteger ) << endl;
		} 
	);
	
	cEventManager.BroadcastEvent( "dave", piData );

	// don't register event, just add it and send
	// note: brian uses a by-value int rather than a pointer
	cEventManager.AddListener( "brian", TestEventCBFloat );

	cEventManager.AddListener( "brian", [] ( float fLoat ){ cout << "lambda! float - " << fLoat; } );

	cEventManager.BroadcastEvent( "brian", (float) iData );


	// last one, to test functions which accept references
	cEventManager.AddListener( "doris", TestEventCBIntRef );

	cEventManager.AddListener( "doris", [&] ( int& ri ){ cout << "lambda! - ref to int" << ri << "iData + ri = " << ( iData + ri ); } );

	cEventManager.BroadcastEvent( "doris", riData );
}
